import app from "./src/app";
const PORT = 3000;

module.exports = app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
})