import * as chai from 'chai';
import * as server from '../server';
import 'chai-http';
import { MockPayments } from '../src/shared/mockPayments';

chai.use(require('chai-http'));

describe('Payment Controller', () => {

    it('should return 200 response code when get all payments route is called', (done) => {
        chai.request(server)
            .get('/payments')
            .end((err, res) => {
                chai.expect(res).have.status(200);
                done();
            });
    });

    it('should return 200 response code when get payment by id route is called and payment is found', (done) => {
        const payment = MockPayments.mockPayments[0];
        chai.request(server)
            .get(`/payments/${payment.getId()}`)
            .end((err, res) => {
                chai.expect(res).have.status(200);
                done();
            });
    });

    it('should return 404 response code when get payment by id route is called and payment is not found', (done) => {
        const paymentId = 'fakeid';
        chai.request(server)
            .get(`/payments/${paymentId}`)
            .end((err, res) => {
                chai.expect(res).have.status(404);
                done();
            });
    });

    it('should return 400 response code when add payment route is called and payment creation fails', (done) => {
        chai.request(server)
            .post(`/payments`)
            .set('Content-type', 'application/json')
            .send({ payment: { 'id': 'paymentId' } })
            .end((err, res) => {
                chai.expect(res).have.status(400);
                done();
            });
    });

    it('should return 200 response code when add payment route is called and payment is created successfully', (done) => {
        chai.request(server)
            .post(`/payments`)
            .set('Content-type', 'application/json')
            .send(
                {
                    payment:
                    {
                        "id": "NfIyWovsTD",
                        "userId": "wWMrA9BRhy",
                        "amount": 123,
                        "stripePaymentId": "M96UFEM2DA"
                    }
                }
            )
            .end((err, res) => {
                chai.expect(res).have.status(200);
                done();
            });
    });

    it('should return 200 response code when update payment route is called and payment is updated successfully', (done) => {
        const payment = MockPayments.mockPayments[0];
        chai.request(server)
            .post(`/payments/${payment.getId()}`)
            .set('Content-type', 'application/json')
            .send(
                {
                    payment:
                    {
                        "userId": "wWMrA9BRhy",
                        "amount": 123,
                        "stripePaymentId": "M96UFEM2DA"
                    }
                }
            )
            .end((err, res) => {
                chai.expect(res).have.status(200);
                done();
            });
    });

    it('should return 400 response code when update payment route is called and payment update fails', (done) => {
        const paymentId = 'fakepaymentid'
        chai.request(server)
            .post(`/payments/${paymentId}`)
            .set('Content-type', 'application/json')
            .send(
                {
                    payment:
                    {
                        "userId": "wWMrA9BRhy",
                        "amount": 123,
                        "stripePaymentId": "M96UFEM2DA"
                    }
                }
            )
            .end((err, res) => {
                chai.expect(res).have.status(400);
                done();
            });
    });

    it('should return 200 response code when delete payment route is called and payment deletion is successful', (done) => {
        const payment = MockPayments.mockPayments[0];
        chai.request(server)
            .delete(`/payments/${payment.getId()}`)
            .end((err, res) => {
                chai.expect(res).have.status(200);
                done();
            });
    });

    it('should return 404 response code when delete payment route is called and payment deletion fails', (done) => {
        const paymentId = 'fakeid';
        chai.request(server)
            .delete(`/payments/${paymentId}`)
            .end((err, res) => {
                chai.expect(res).have.status(404);
                done();
            });
    });
});