import { Payment } from "../models/payment";

export class MockPayments {
    public static readonly mockPayments: Payment[] = [
        new Payment('NfIyWovsTD', 'wWMrA9BRhy', 234.2, 'M96UFEM2DA'),
        new Payment('Xwx6DueKh6', 'gROo2OW1Ii', 434.2, 'gROo2OW1Ii'),
    ];

}