import * as express from "express";
import * as bodyParser from "body-parser";
import * as routes from "./routes";
import { Routes } from "./routes";


class App {
    public app: express.Application = express();
    public routesPrv: Routes = new Routes();

    constructor() {
        this.config();
        this.routesPrv.routes(this.app);
    }

    private config(): void {
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(bodyParser.json());
    }
}

export default new App().app;
