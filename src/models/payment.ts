export class Payment {
    private id: string;
    private userId: string;
    private amount: number;
    private stripePaymentId: string;

    constructor(id: string, userId: string, amount: number, stripePaymentId: string) {
        this.id = id;
        this.userId = userId;
        this.amount = amount;
        this.stripePaymentId = stripePaymentId;
    }

    public getId(): string {
        return this.id;
    }

    public toJSON(): Object {
        return {
            'id': this.id,
            'userId': this.userId,
            'amount': this.amount,
            'stripePaymentId': this.stripePaymentId
        };
    }
}