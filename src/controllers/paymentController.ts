import { Request, Response } from 'express';
import { MockPayments } from '../shared/mockPayments';
import { Payment } from '../models/payment';
export class PaymentController {

    public getAllPayments(req: Request, res: Response) {
        const payments = MockPayments.mockPayments.map((payment) => payment.toJSON());
        res.status(200).json(payments);
    }

    public getPaymentByID(req: Request, res: Response) {
        const paymentId = req.params.id;
        const foundPayment = MockPayments.mockPayments.find((payment) => payment.getId() === paymentId);
        if (foundPayment) {
            res.status(200).json(foundPayment);
            return;
        }
        res.status(404).json({ error: 'Not found' });

    }

    public addPayment(req: Request, res: Response) {
        const jsonPayment = req.body.payment;
        if (jsonPayment && jsonPayment.userId && jsonPayment.amount && jsonPayment.stripePaymentId) {
            // Add payment - DB
            const payment = new Payment('payment_id_db', jsonPayment.userId, jsonPayment.amount, jsonPayment.stripePaymentId);
            res.status(200).json(payment.toJSON());
            return;
        }
        res.status(400).json({ error: 'Payment creation failed!' });

    }

    public updatePayment(req: Request, res: Response) {
        const paymentId = req.params.id;
        const jsonPayment = req.body.payment;
        if (paymentId && jsonPayment && jsonPayment.userId && jsonPayment.amount && jsonPayment.stripePaymentId) {
            const paymentObj = new Payment(paymentId, jsonPayment.userId, jsonPayment.amount, jsonPayment.stripePaymentId);

            const foundPayment = MockPayments.mockPayments.find((payment) => payment.getId() === paymentId);
            if (foundPayment) {
                // Update payment - DB
                res.status(200).json(paymentObj.toJSON()).end();
                return;
            }
        }
        res.status(400).json({ error: 'Payment update failed!' });
    }

    public deletePayment(req: Request, res: Response) {
        const paymentId = req.params.id;
        const foundPayment = MockPayments.mockPayments.find((payment) => payment.getId() === paymentId);
        if (foundPayment) {
            // Delete payment - DB
            res.status(200).json(foundPayment);
            return;
        }
        res.status(404).json({ error: 'Payment deletion failed!' });

    }


}