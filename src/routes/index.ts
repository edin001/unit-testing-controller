import { PaymentController } from '../controllers/paymentController';

export class Routes {
    public paymentController: PaymentController = new PaymentController();
    public routes(app): void {
        app.route('/payments')
            .get(this.paymentController.getAllPayments)
            .post(this.paymentController.addPayment);

        app.route('/payments/:id')
            .get(this.paymentController.getPaymentByID)
            .post(this.paymentController.updatePayment)
            .delete(this.paymentController.deletePayment);

    }
}